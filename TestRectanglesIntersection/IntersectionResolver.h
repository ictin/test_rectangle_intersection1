#pragma once
#include <vector>
#include "Rectangle.h"

class IntersectionResolver
{
public:
    IntersectionResolver();
    ~IntersectionResolver();
    void ResolveRectangle(Rectangle *newRectangle);
    void Finalize();
    const std::vector<Rectangle*> GetIntersectionResult() const;

private:
    std::vector<Rectangle*> _intersectionResult;
    void RemoveRectangle(Rectangle *r, bool erase = true);
    void AddRectangle(Rectangle *r);
    std::vector<Rectangle*> _currentRectangles;    
};


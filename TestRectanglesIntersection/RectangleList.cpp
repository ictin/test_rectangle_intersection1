#include "RectangleList.h"
#include <algorithm>
#include <fstream>


bool RectangleComparison(Rectangle* r1, Rectangle* r2) { return (r1->X() < r2->X()); }

RectangleList::RectangleList()
{
}


RectangleList::~RectangleList()
{
    for (Rectangle* r : _list)
    {
        delete r;
    }

    _list.clear();
}

void RectangleList::AddRectangle(Rectangle* item)
{
    item->RectangleId(_list.size() + 1);
    _list.push_back(item);
}

void RectangleList::PrintRectangles()
{
    printf("Input:\n");
    for (Rectangle* r: _list)
    {
        printf("\t%d: Rectangle at (%d,%d) w=%d, h=%d.\n", r->RectangleId(), r->X(), r->Y(), r->W(), r->H());
    }
}

void RectangleList::ResolveIntersection()
{
    SortList();
    for (Rectangle *r:_list)
    {
        _resolver.ResolveRectangle(r);
    }
    _resolver.Finalize();
}

void RectangleList::PrintIntersection()
{
    printf("\nIntersections:\n");
    for (Rectangle* r: _resolver.GetIntersectionResult())
    {
        printf("\tBetween rectangles %d", r->RectangleId());        
        auto &last = *(--r->Intersections().end());
        for (int id : r->Intersections())
        {
            if (id != last)
            {
                printf(", %d", id);
            }
            else
            {
                printf(" and %d", id);
            }
        } 
        printf(" at(%d, %d), w=%d, h=%d.\n", r->X(), r->Y(), r->W(), r->H());
    }
}

int RectangleList::ReadFromFile(char* filename)
{
    int status = 0;
    // read a JSON file
    nlohmann::json j;
    try
    {
        std::ifstream i(filename);        
        i >> j;        
    }
    catch(...)
    {
        status = -1;
    }

    if(!status)
    {
        auto jsonList = j["rects"];
        if (jsonList != nullptr)
        {
            int count = 0;
            for (auto r : jsonList)
            {
                if(r["x"] == nullptr || r["y"] == nullptr || r["w"] == nullptr || r["h"] == nullptr)
                {
                    return -2;
                }
                Rectangle* newRectangle = new Rectangle(r["x"], r["y"], r["w"], r["h"]);
                this->AddRectangle(newRectangle);
                count++;
                if(count >= 1000)
                {
                    break;
                }
            }
        }
        else
        {
            status = -2;
        }
    }
    return status;
}

void RectangleList::SortList()
{
    sort(_list.begin(), _list.end(), RectangleComparison);
}

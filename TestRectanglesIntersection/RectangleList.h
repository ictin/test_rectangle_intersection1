#pragma once
#include <vector>
#include "Rectangle.h"
#include "IntersectionResolver.h"
#include "json.hpp"

class RectangleList
{
public:
    RectangleList();
    ~RectangleList();
    void AddRectangle(Rectangle* item);
    void PrintRectangles();
    void ResolveIntersection();
    void PrintIntersection();
    int ReadFromFile(char* filename);

private:
    void SortList();
    std::vector<Rectangle*> _list;
    IntersectionResolver _resolver;
};


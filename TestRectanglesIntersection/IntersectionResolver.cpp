#include "IntersectionResolver.h"
#include <algorithm>

bool RectangleIntersectionComparison(Rectangle* r1, Rectangle* r2)
{
    if (r1->Intersections().size() == r2->Intersections().size())
    {
        return (r1->RectangleId() < r2->RectangleId());
    }
    else
    {
        return r1->Intersections().size() < r2->Intersections().size();
    }
}

IntersectionResolver::IntersectionResolver()
{
}


IntersectionResolver::~IntersectionResolver()
{
    for (Rectangle* r : _intersectionResult)
    {
        delete r;
    }

    _intersectionResult.clear();
}

void IntersectionResolver::ResolveRectangle(Rectangle* newRectangle)
{
    std::vector<Rectangle*> removeRectangles;
    std::vector<Rectangle*> addRectangles;
    for(Rectangle* r: _currentRectangles)
    {
        if(r->X()+r->W() < newRectangle->X())
        {
            removeRectangles.push_back(r);
        }
        else
        {
            Rectangle *result = r->IntersectionResult(newRectangle);
            if(result)
            {
                addRectangles.push_back(result);
            }
        }        
    }

    for(Rectangle *r:removeRectangles)
    {
        RemoveRectangle(r);
    }

    for (Rectangle *r : addRectangles)
    {
        AddRectangle(r);
    }

    AddRectangle(newRectangle);
}

void IntersectionResolver::Finalize()
{
    for (Rectangle* r : _currentRectangles)
    {
        RemoveRectangle(r, false);
    }
    _currentRectangles.clear();

    std::sort(_intersectionResult.begin(), _intersectionResult.end(), RectangleIntersectionComparison);
}

const std::vector<Rectangle*> IntersectionResolver::GetIntersectionResult() const
{
    return _intersectionResult;
}

void IntersectionResolver::RemoveRectangle(Rectangle* r, bool erase)
{
    if(r->IsResultRectangle())
    {
        _intersectionResult.push_back(r);
    }
    if (erase)
    {
        const auto it = std::find(_currentRectangles.begin(), _currentRectangles.end(), r);
        if (it != _currentRectangles.end())
        {
            _currentRectangles.erase(it);
        }

    }
}

void IntersectionResolver::AddRectangle(Rectangle* r)
{
    _currentRectangles.push_back(r);
}

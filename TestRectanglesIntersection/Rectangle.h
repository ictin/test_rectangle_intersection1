#pragma once
#include <vector>

class Rectangle
{
public:
    Rectangle(int x, int y, int w, int h);
    ~Rectangle();
    int X() const;
    int Y() const;
    unsigned W() const;
    unsigned H() const;

    void RectangleId(int id);
    int RectangleId() const;

    bool IsResultRectangle() const;
    void IsResultRectangle(bool value);

    const std::vector<int>& Intersections() const;
	void AddNewIntersectionPoint(int id);
    Rectangle *IntersectionResult(Rectangle *target) const;

private:
    int _x, _y;
    unsigned int _w, _h;
    unsigned int _rectangleId;
    bool _isResultRectangle;
	std::vector<int> _intersectIds;
};


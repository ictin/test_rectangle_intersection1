#include "RectangleList.h"

int main(int argc, char **argv)
{
    if(argc != 2)
    {
        return 1;
    }
    RectangleList list;
    const int readStatus = list.ReadFromFile(argv[1]);
    if (!readStatus)
    {
        list.PrintRectangles();
        list.ResolveIntersection();
        list.PrintIntersection();
    }
    else
    {
        if(readStatus == -1)
        {
            printf("The specified file can't be read\n");
        }
        else if(readStatus == -2)
        {
            printf("The specified file doesn't contain valid JSON data\n");
        }

        return readStatus;
    }

    return 0;     
}
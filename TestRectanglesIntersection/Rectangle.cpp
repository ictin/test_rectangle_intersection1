 #include "Rectangle.h"
#include <cstdlib>
#define max(x,y) (x>y) ? x : y
#define min(x,y) (x<y) ? x : y

Rectangle::Rectangle(int x, int y, int w, int h)
    :_x(x)
    ,_y(y)
    ,_w(w)
    ,_h(h)
    , _rectangleId(0)
    , _isResultRectangle(false)
{
}

Rectangle::~Rectangle()
{
}

void Rectangle::RectangleId(int id)
{
    _rectangleId = id;
}

int Rectangle::RectangleId() const
{
    return _rectangleId;
}

const std::vector<int>& Rectangle::Intersections() const
{
	return _intersectIds;
}

void Rectangle::AddNewIntersectionPoint(int id)
{
	_intersectIds.push_back(id);
}

int Rectangle::X() const
{
    return _x;
}

int Rectangle::Y() const
{
    return _y;
}

unsigned Rectangle::W() const
{
    return _w;
}

unsigned Rectangle::H() const
{
    return _h;
}

bool Rectangle::IsResultRectangle() const
{
    return _isResultRectangle;
}

void Rectangle::IsResultRectangle(bool value)
{
    _isResultRectangle = value;
}

Rectangle* Rectangle::IntersectionResult(Rectangle* target) const
{
    Rectangle* result = nullptr;
    if(_rectangleId == target->RectangleId())
    {
        return nullptr;
    }

	const int x1 = max(_x, target->X());
	const int x2 = min(_x + _w, target->X() + target->W());
	const int y1 = max(_y, target->Y());
	const int y2 = min(_y + _h, target->Y() + target->H());

	if( (x1 < x2) && (y1 < y2))
	{
		result = new Rectangle(x1, y1, x2 - x1, y2 - y1);

        for(int id:this->_intersectIds)
        {
            result->AddNewIntersectionPoint(id);
        }
		result->AddNewIntersectionPoint(target->RectangleId());
		result->RectangleId(_rectangleId);
        result->IsResultRectangle(true);
	}
	
    return  result;
}
